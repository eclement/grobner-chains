# Grobner-chains

Description of the modules:

- monomials.ml implements the behaviour of the tuple (coeff, mdeg) where 
coeff is a rational number and mdeg is a vector of natural number.
This represent a multivariable monomial joint with its coefficient.
It provides basic operations (multiplication, addition etc).

- polynomials.ml implements multivarieted polynomial in the field of rational
numbers, euclidien division by one or by a list of ordered polynomials.

- buchberger.ml implements the computation of grobner basis.