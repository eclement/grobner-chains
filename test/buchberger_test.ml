module M = Buch.Monomial.MkMonomial (struct
  let dim = 2
  let var_name = [| "x"; "y" |]
end)

(* Creating K[X,Y] *)
module MonoLex = Buch.Monomial.MkLexOrderedMonomial (M)
module MonoGLex = Buch.Monomial.MkGradLexOrderedMonomial (M)
module PLex = Buch.Polynomial.MkPolynomial (MonoLex)
module PGLex = Buch.Polynomial.MkPolynomial (MonoGLex)
module BLex = Buch.Buchberger.MkBuchberger (PLex)
module BGLex = Buch.Buchberger.MkBuchberger (PGLex)

(* Example from cox please *)

(* A first gentle example on k[x,y] *)
let () = ()
(* Constant monomials *)
(* Printf.printf "coucou0\n%!";
   let minus_one = M.make (Q.neg Q.one) [| 0; 0 |] in
   let one = M.make Q.one [| 0; 0 |] in
   (*Printf.printf "coucou1\n%!";*)
   (* Monomials on x only *)
   let x = M.make Q.one [| 1; 0 |] in
   let _minus_x = M.multiply minus_one x in
   let x3 = M.make Q.one [| 3; 0 |] in
   let _minus_x3 = M.multiply minus_one x3 in
   let x2 = M.make Q.one [| 2; 0 |] in
   let minus_x2 = M.multiply minus_one x2 in
   (* Printf.printf "coucou2\n%!";*)
   (* Monomials on y only *)
   let y = M.make Q.one [| 0; 1 |] in
   let _minus_y = M.multiply minus_one y in
   let y2 = M.make Q.one [| 0; 2 |] in
   let _minus_y2 = M.multiply minus_one y2 in
   let y3 = M.make Q.one [| 0; 3 |] in
   let _minus_y3 = M.multiply minus_one y3 in

   let minus_2_y2 =
     M.make (Q.neg Q.{ num = Z.of_int 2; den = Z.one }) [| 0; 2 |]
   in
   (*Printf.printf "coucou3\n%!";*)
   (* Monomial of the form xy^alpha*)
   let x_y2 = M.make Q.one [| 1; 2 |] in

   (* Monomials of the form x^alpha * y*)
   let x2_y = M.make Q.one [| 2; 1 |] in
   (* Monomials with x^a y^a *)
   let x_y = M.make Q.one [| 1; 1 |] in
   let minus_2_xy =
     M.make (Q.neg Q.{ num = Z.of_int 2; den = Z.one }) [| 1; 1 |]
   in

   (* Monomials with x^a y^b, b<a*)
   let x3_y2 = M.make Q.one [| 3; 2 |] in
   let three_x4_y = M.make Q.{ num = Z.of_int 3; den = Z.one } [| 4; 1 |] in
   (*Printf.printf "coucou4\n%!";*)
   (* Monomials with x^a y^b , b > a*)
   let minus_x2_y3 = M.make (Q.neg Q.one) [| 2; 3 |] in
   let minus_2_xy2 = M.make Q.{ num = Z.of_int (-2); den = Z.one } [| 1; 2 |] in
   (*Printf.printf "coucou5\n%!";*)
   (* Creation of polynomials *)
   let f_1 = [ x3; minus_2_xy ] in
   let f_2 = [ x2_y; minus_2_y2; x ] in
   let f_3 = [ minus_x2 ] in
   let f_4 = [ minus_2_xy ] in
   let f_5 = [ minus_2_y2; x ] in
   let f_6 = [ x3; x2; one ] in
   let f_7 = [ x; one ] in
   let f_8 = [ x2_y; x_y2; y2 ] in
   let f_9 = [ x_y; minus_one ] in
   let f_1_0 = [ y2; minus_one ] in
   let f_1_1 = [ x2_y; x_y2 ] in
   let f_1_2 = [ x_y2 ] in
   (*S-poly example: p85*)
   let f_buch_1 = [ x3_y2; minus_x2_y3; x ] in
   let g_buch_1 = [ three_x4_y; y2 ] in

   (*S-poly example p90*)
   let f_1_poly_ex_90 = PGLex.make [ x3; minus_2_xy ] in
   let f_2_poly_ex_90 = PGLex.make [ x2_y; minus_2_y2; x ] in
   let f_3_poly_ex_90 = PGLex.make [ minus_x2 ] in
   let f_4_poly_ex_90 = PGLex.make [ minus_2_xy ] in
   let f_5_poly_ex_90 = PGLex.make [ minus_2_y2; x ] in
   let s_1_2 = BGLex.s_poly f_1_poly_ex_90 f_2_poly_ex_90 in
   let s_1_3 = BGLex.s_poly f_1_poly_ex_90 f_3_poly_ex_90 in
   let s_1_4 = BGLex.s_poly f_1_poly_ex_90 f_4_poly_ex_90 in
   let s_2_3 = BGLex.s_poly f_2_poly_ex_90 f_3_poly_ex_90 in
   let test_s_poly_f1_f2 = PGLex.eq s_1_2 f_3_poly_ex_90 in
   let test_s_poly_f1_f3 = PGLex.eq s_1_3 f_4_poly_ex_90 in
   let test_s_poly_f1_f4 = PGLex.eq s_1_4 (PGLex.make [ minus_2_xy2 ]) in
   let test_s_poly_f2_f3 = PGLex.eq s_2_3 f_5_poly_ex_90 in
   (*Printf.printf "coucou6\n%!";*)
   let f_buch_list =
     [
       f_1_poly_ex_90;
       f_2_poly_ex_90;
       f_3_poly_ex_90;
       f_4_poly_ex_90;
       f_5_poly_ex_90;
     ]
   in
   let _, r_0 = PGLex.multi_euclidian_div s_1_2 f_buch_list in
   let _, r_1 = PGLex.multi_euclidian_div s_1_3 f_buch_list in
   let _, r_2 = PGLex.multi_euclidian_div s_1_4 f_buch_list in
   let _, r_3 = PGLex.multi_euclidian_div s_2_3 f_buch_list in

   (*Printf.printf "coucou7\n%!";*)
   let test_eucl_div =
     PGLex.is_zero r_0 && PGLex.is_zero r_1 && PGLex.is_zero r_2
     && PGLex.is_zero r_3
   in
   Printf.printf "euclidian div should return 0 :%s\n%!"
     (if test_eucl_div then "true" else "false");
   Printf.printf "First s_poly test for f_1 , f_2 :%s\n%!"
     (if test_s_poly_f1_f2 then "true" else "false");
   Printf.printf "First s_poly test for f_1 , f_3 :%s\n%!"
     (if test_s_poly_f1_f3 then "true" else "false");
   Printf.printf "First s_poly test for f_1 , f_4 :%s\n%!"
     (if test_s_poly_f1_f4 then "true" else "false");
   Printf.printf "First s_poly test for f_2 , f_3 :%s\n%!"
     (if test_s_poly_f2_f3 then "true" else "false");

   (* let basis =  *)
   let ( f_1_lex,
         f_2_lex,
         _f_3_lex,
         _f_4_lex,
         _f_5_lex,
         _f_6_lex,
         _f_7_lex,
         f_8_lex,
         f_9_lex,
         f_1_0_lex,
         f_1_1_lex,
         f_1_2_lex,
         f_buch_1_lex,
         g_buch_1_lex ) =
     ( PGLex.make f_1,
       PGLex.make f_2,
       PGLex.make f_3,
       PGLex.make f_4,
       PGLex.make f_5,
       PGLex.make f_6,
       PGLex.make f_7,
       PGLex.make f_8,
       PGLex.make f_9,
       PGLex.make f_1_0,
       PGLex.make f_1_1,
       PGLex.make f_1_2,
       PGLex.make f_buch_1,
       PGLex.make g_buch_1 )
   in

   let _f_1_glex, _f_2_glex, _f_3_glex, _f_4_glex, _f_5_glex =
     ( PGLex.make f_1,
       PGLex.make f_2,
       PGLex.make f_3,
       PGLex.make f_4,
       PGLex.make f_5 )
   in
   (*Printf.printf "coucou8\n%!";*)
   (* Test cartesian product *)
   (* S-poly computation *)
   let _s_poly_1 = BGLex.s_poly f_buch_1_lex g_buch_1_lex in
   (* Works*)
   (* Printf.printf "s_poly= %a\n" PGLex.pp s_poly_1; *)
   (* Classic buchberger *)
   let poly_list = [ f_1_lex; f_2_lex ] in
   let q, r = PGLex.euclidian_div f_1_1_lex f_1_2_lex in
   (* Printf.printf "lt f_1 = %a\n" M.pp_monomial (PGLex.lt f_1_lex);
      Printf.printf "f_1 = %a\n" PGLex.pp f_1_lex; *)
   Printf.printf "q = %a\nr = %a%!" PGLex.pp q PGLex.pp r;
   let f_list = [ f_9_lex; f_1_0_lex ] in
   let q_list, r_res = PGLex.multi_euclidian_div f_8_lex f_list in
   Printf.printf "les quotient sont q=[%!";
   List.iter (fun x -> Printf.printf "%a\n %!" PGLex.pp x) q_list;
   Printf.printf "]\nr=%!";
   Printf.printf "r = %a\n%!" PGLex.pp r_res;
   (*Printf.printf "coucou9\n%!";*)
   let b = BGLex.buchberger poly_list in
   Printf.printf "]\n first buchberger results=\n%!";
   List.iter (fun x -> Printf.printf "poly=%a\n %!" PGLex.pp x) b;
   () *)

(* Optimized buchberger *)
