module M = Buch.Monomial.MkMonomial (struct
  let dim = 3
  let var_name = [| "x"; "y"; "z" |]
end)

(* Creating K[X,Y] *)
module MonoLex = Buch.Monomial.MkLexOrderedMonomial (M)
module MonoGLex = Buch.Monomial.MkGradLexOrderedMonomial (M)
module PLex = Buch.Polynomial.MkPolynomial (MonoLex)
module PGLex = Buch.Polynomial.MkPolynomial (MonoGLex)
module BLex = Buch.Buchberger.MkBuchberger (PLex)
module BGLex = Buch.Buchberger.MkBuchberger (PGLex)

(* Example from cox please *)

(* A first gentle example on k[x,y] *)
let () =
  (* Constant monomials *)
  let open Format in
  let open M in
  let module P = PGLex in
  let open P in
  printf "coucou0\n%!";
  let minus_one = P.of_mono Q.minus_one M.cst in
  let one = P.of_mono Q.one M.cst in
  (* printf "coucou1\n%!";*)
  (* Monomials on x only *)
  let x = one_var 0 in
  let y = one_var 1 in
  let z = one_var 2 in
  let minus_2 = Q.neg (Q.of_int 2) in

  (* printf "coucou2\n%!";*)
  (* Monomials on y only *)
  let minus_2_y2 = P.(minus_2 *: y) in
  (*Format.printf "coucou3\n%!";*)
  (* Monomial of the form xy^alpha*)
  let x_y2 = M.(x * (y ** 2)) in

  (* Monomials of the form x^alpha * y*)
  let x2_y = M.((x ** 2) * y) in
  (* Monomials with x^a y^a *)
  let xy = M.(x * y) in
  let minus_2_xy = P.(minus_2 *: (x * y)) in

  (* Monomials with x^a y^b, b<a*)
  let x3_y2 = M.((x ** 3) * (y ** 2)) in
  let three_x4_y = P.(Q.of_int 3 *: ((x ** 4) * y)) in
  (* printf "coucou4\n%!";*)
  (* Monomials with x^a y^b , b > a*)
  let minus_x2_y3 = P.(Q.minus_one *: ((x ** 2) * (y ** 3))) in
  let minus_2_xy2 = minus_2 *: (x * (y ** 2)) in
  (* printf "coucou5\n%!";*)
  (* Creation of polynomials *)
  let f_1 = (Q.one *: (x ** 3)) - (Q.of_int 2 *: xy) in
  let f_2 = (Q.one *: x2_y) + minus_2_y2 + (Q.one *: x) in
  let f_3 = Q.minus_one *: (x ** 2) in
  let f_4 = minus_2_xy in
  let f_5 = minus_2_y2 + (Q.one *: x) in
  let f_6 = (Q.one *: (x ** 3)) + (Q.one *: (x ** 2)) + one in
  let f_7 = (Q.one *: x) + one in
  let f_8 = (Q.one *: x2_y) + (Q.one *: x_y2) + (Q.one *: (y ** 2)) in
  let f_9 = (Q.one *: xy) + minus_one in
  let f_1_0 = (Q.one *: (y ** 2)) + minus_one in
  let f_1_1 = (Q.one *: x2_y) + (Q.one *: x_y2) in
  let f_1_2 = Q.one *: x_y2 in
  (*S-poly example: p85*)
  let f_buch_1 = (Q.one *: x3_y2) + minus_x2_y3 + (Q.one *: x) in
  let g_buch_1 = three_x4_y + (Q.one *: (y ** 2)) in

  (*S-poly example p90*)
  let f_1_poly_ex_90 = f_1 in
  let f_2_poly_ex_90 = f_2 in
  let f_3_poly_ex_90 = f_3 in
  let f_4_poly_ex_90 = f_4 in
  let f_5_poly_ex_90 = f_5 in
  let s_1_2 = BGLex.s_poly f_1_poly_ex_90 f_2_poly_ex_90 in
  let s_1_3 = BGLex.s_poly f_1_poly_ex_90 f_3_poly_ex_90 in
  let s_1_4 = BGLex.s_poly f_1_poly_ex_90 f_4_poly_ex_90 in
  let s_2_3 = BGLex.s_poly f_2_poly_ex_90 f_3_poly_ex_90 in
  let test_s_poly_f1_f2 = P.eq s_1_2 f_3_poly_ex_90 in
  let test_s_poly_f1_f3 = P.eq s_1_3 f_4_poly_ex_90 in
  let test_s_poly_f1_f4 = P.eq s_1_4 minus_2_xy2 in
  let test_s_poly_f2_f3 = P.eq s_2_3 f_5_poly_ex_90 in
  (*Format.printf "coucou6\n%!";*)
  let f_buch_list =
    [
      f_1_poly_ex_90;
      f_2_poly_ex_90;
      f_3_poly_ex_90;
      f_4_poly_ex_90;
      f_5_poly_ex_90;
    ]
  in
  let _, r_0 = P.multi_euclidian_div s_1_2 f_buch_list in
  let _, r_1 = P.multi_euclidian_div s_1_3 f_buch_list in
  let _, r_2 = P.multi_euclidian_div s_1_4 f_buch_list in
  let _, r_3 = P.multi_euclidian_div s_2_3 f_buch_list in

  (*Format.printf "coucou7\n%!";*)
  let test_eucl_div =
    P.is_zero r_0 && P.is_zero r_1 && P.is_zero r_2 && P.is_zero r_3
  in
  Format.printf "euclidian div should return 0 :%s\n%!"
    (if test_eucl_div then "true" else "false");
  Format.printf "First s_poly test for f_1 , f_2 :%s\n%!"
    (if test_s_poly_f1_f2 then "true" else "false");
  Format.printf "First s_poly test for f_1 , f_3 :%s\n%!"
    (if test_s_poly_f1_f3 then "true" else "false");
  Format.printf "First s_poly test for f_1 , f_4 :%s\n%!"
    (if test_s_poly_f1_f4 then "true" else "false");
  Format.printf "First s_poly test for f_2 , f_3 :%s\n%!"
    (if test_s_poly_f2_f3 then "true" else "false");

  (* let basis =  *)
  let ( f_1_lex,
        f_2_lex,
        _f_3_lex,
        _f_4_lex,
        _f_5_lex,
        _f_6_lex,
        _f_7_lex,
        f_8_lex,
        f_9_lex,
        f_1_0_lex,
        f_1_1_lex,
        f_1_2_lex,
        f_buch_1_lex,
        g_buch_1_lex ) =
    ( f_1,
      f_2,
      f_3,
      f_4,
      f_5,
      f_6,
      f_7,
      f_8,
      f_9,
      f_1_0,
      f_1_1,
      f_1_2,
      f_buch_1,
      g_buch_1 )
  in

  let _f_1_glex, _f_2_glex, _f_3_glex, _f_4_glex, _f_5_glex =
    (f_1, f_2, f_3, f_4, f_5)
  in
  (*Format.printf "coucou8\n%!";*)
  (* Test cartesian product *)
  (* S-poly computation *)
  let _s_poly_1 = BGLex.s_poly f_buch_1_lex g_buch_1_lex in
  (* Works*)
  (* Format.printf "s_poly= %a\n" PGLex.pp s_poly_1; *)
  (* Classic buchberger *)
  let poly_list = [ f_1_lex; f_2_lex ] in
  let q, r = P.euclidian_div f_1_1_lex f_1_2_lex in
  (* Format.printf "lt f_1 = %a\n" M.pp_monomial (PGLex.lt f_1_lex);
     Format.printf "f_1 = %a\n" PGLex.pp f_1_lex; *)
  Format.printf "q = %a\nr = %a\n%!" P.pp q P.pp r;
  let f_list = [ f_9_lex; f_1_0_lex ] in
  let q_list, r_res = P.multi_euclidian_div f_8_lex f_list in
  Format.printf "les quotient sont q=[%!";
  List.iter (fun x -> Format.printf "%a\n %!" P.pp x) q_list;
  Format.printf "]\n%!";
  Format.printf "r = %a\n%!" P.pp r_res;
  (*Format.printf "coucou9\n%!";*)
  let b = BGLex.buchberger poly_list in
  Format.printf "]\n first buchberger results=\n%!";
  List.iter (fun x -> Format.printf "poly=%a\n %!" P.pp x) b;

  (*New example with three variables polynomials *)

  (* Monomials we need *)
  let xz = M.(x * z) in
  let yz2 = M.(y * (z ** 2)) in

  let minus_z2 = Q.minus_one *: (z ** 2) in

  let f_1_ex_2 =
    (Q.one *: (x ** 3)) + minus_2_xy + (Q.one *: (z ** 3)) + (Q.one *: xz)
  in
  let f_2_ex_2 = (Q.one *: x2_y) + minus_2_y2 + (Q.one *: z) in
  let f_3_ex_2 = (Q.one *: yz2) + minus_z2 in

  let poly_list_ex_2 = [ f_1_ex_2; f_2_ex_2; f_3_ex_2 ] in
  let b2 = BGLex.buchberger poly_list_ex_2 in
  Format.printf "]\n buchberger results for ex_2 =\n%!";
  List.iter (fun x -> Format.printf "poly=%a\n %!" P.pp x) b2;

  ()

(* Optimized buchberger *)
