(* val is_zero : t -> bool *)
(** verifies if a polynomial values zero *)

(* Verify empty list, non empty list *)

(* val make : Monomial.t list -> t *)
(** transforms a list of monomials into a polynomial *)

(* Test similar polynomial that should merge, but with different position : next to each other, far away, begin, end...*)

(* val addition : t -> t -> t *)
(** returns the addition of two polynomials *)

(* Test when should merge several time, no time, empty list, and different mdeg polynomial *)

(* val multiply : t -> t -> t *)
(** returns the multiplication of two polynomials *)

(* Test example of book *)

(* val lt : t -> Monomial.t *)
(** returns the leading term of a polynomial t *)

(* Test example of book *)

(* val lc : t -> Q.t *)
(** returns the coefficient of the leading term of a polynomial t *)

(* Test example of book *)

(* val multideg : t -> Monomial.mdeg *)
(** returns the leading mdeg of a polynomial t *)

(* Test example of book + negative values (even if its stupid) *)

(* val multideg_of_mul : t -> t -> Monomial.mdeg *)
(** computes the mdeg of the multiplication of two polynomials *)

(* val euclidian_div : t -> t -> t * t *)
(** returns (quotient, remainder) of the euclidian division of two polynomials *)

(* Test example of book *)

(* val multi_euclidian_div : t -> t list -> t list * t *)
(** returns (quotients, remainder) of the euclidien division of a polynomials by a list of well-ordered polynomials *)

(* Test example of book *)

(* val eq : t -> t -> bool *)

(* Test example of book *)

(* val sort : t list -> t list *)

(* Test example of book *)
