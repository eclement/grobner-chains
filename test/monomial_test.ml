module M = Buch.Monomial.MkMonomial (struct
  let dim = 3
  let var_name = [| "x"; "y"; "z" |]
end)

(* let () =
   let mdegzero = M.cst in
   let mdeg1 = M.make Q.zero [| 0; 1; -1 |] in
   let mdeg2 = M.make Q.one [| 0; 1; -1 |] in

   let test_zero_0 = M.is_zero mdegzero in
   let test_zero_1 = M.is_zero mdeg1 in
   let test_zero_2 = M.is_zero mdeg2 in

   (* Test is_zero *)
   Printf.printf "Test of is_zero function";
   Printf.printf "\n";
   Printf.printf "%b" test_zero_0;
   Printf.printf "\n";
   Printf.printf "%b" test_zero_1;
   Printf.printf "\n";
   Printf.printf "%b" test_zero_2;

   (* monomials from cox*)

   (* Test lexo_compare *)
   Printf.printf "\n";
   Printf.printf "Test of lexicographic compare" *)

(*Printf.printf "%i" M.is_divisible *)
(*
let print_array arr =
  let open Printf in
  printf "[|";
  Array.iter (fun x -> printf "%d; " x) arr;
  printf "|]"*)

(*
let print_array_array arr =
  let open Printf in
  printf "[|";
  Array.iter
    (fun m ->
      printf "\t";
      print_array m;
      printf ";\n")
    arr;
  printf "|]"
*)

(* let () =
   let d1 = M.{ coeff = Q.one; mdeg = [| 0; 2; 4; 6; 3; 6; 5; 5; 9; 8 |] } in
   let d2 = M.{ coeff = Q.one; mdeg = [| 7; 8; 8; 6; 4; 2; 5; 1; 3; 1 |] } in
   let diff = M.lexicographic_compare d1 d2 in
   Printf.printf "%d\n" diff *)

(* Unit test: *)

(* Creation of monomials needed *)

(* Test on functions of monomial.ml *)

(* val degree : t -> mdeg *)
(** returns the degree mdeg of a monomial *)

(* Just test empty, 1 and 2, value 0 or not. Test negative value to accept them (depsite its stupidity )*)

(*  val is_zero : t -> bool *)
(** verifies if a monomial values zero *)

(* An example with 0 and 1 et un fractionelle as coeff, with or without mdeg = (0 0 0 0 0) *)

(* val is_divisible : t -> t -> bool * mdeg *)
(** verifies if a monomial m1 divides another one m2, and return (true, quotient) if so, and (false, quotient) otherwise, where quotient = m1/m2 *)

(* An example where divide *)

(* An example where not dividing *)

(* an example where divise, with =/= coeff *)

(* An example with a monomial valuing 0, 1 *)

(* val lexicographic_compare : t -> t -> int *)
(** compares with lexical order two polynomials *)

(* Tester des examples avec liste vide, liste length 1, doublon, tout coeff identique *)

(* val gradlex_compare : t -> t -> int *)
(** compares with Graded Lex order two polynomials *)

(* Tester les même exemples, tester un exemple qui renvoie au lex compare *)

(* val mdeg_add : mdeg -> mdeg -> mdeg *)
(** additions two mdeg *)

(* Use stupid example *)

(* val mdeg_max : mdeg -> mdeg -> mdeg *)
(** returns the maximum (coefficient by coefficient) of two mdeg *)

(* Use stupid example *)

(* val total_deg : mdeg -> int *)
(** returns the sum of coefficients of the mdeg *)

(* Same *)

(* val is_equal_mdeg : mdeg -> mdeg -> bool *)
(** verifies equality of two mdeg *)

(* Verify a not, an empty, same length *)

(* val is_equal : t -> t -> bool *)
(** verifies the equality of two monomials *)

(* Verify a not, an empty, same length *)

(* val addition : t -> t -> t *)
(** returns the additions of two monomials *)

(* Verify example giving negative values, sum =0 , negative mdeg (stupid but ...)*)

(* val multiply : t -> t -> t *)
(** returns the multiplication of two monomials *)

(* Same *)
