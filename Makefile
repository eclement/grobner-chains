build:
	dune build @fmt --auto-promote

build-docker: 
	docker build -t grobner ./

run-docker: build-docker
	docker run --rm grobner bucherberger_3_test.exe