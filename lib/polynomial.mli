module type Poly = sig
  module Mono : Monomial.OrderedMonomial

  type cm = { coeff : Q.t; mdeg : Mono.t }
  type t (* DO NOT EXPOSE !!*)

  val of_mono : Q.t -> Mono.t -> t
  val zero : t

  val is_zero : t -> bool
  (** verifies if a polynomial values zero *)

  val eq : t -> t -> bool

  val lt : t -> cm
  (** returns the leading term of a polynomial t *)

  val lc : t -> Q.t
  (** returns the coefficient of the leading term of a polynomial t *)

  val lm : t -> Mono.t
  (** returns the leading mdeg of a polynomial t *)

  val prod_coef : Q.t -> t -> t
  val mult_mono : Mono.t -> t -> t

  val mult_coef_mono : cm -> t -> t
  (** returns the multiplication of a polynomial with a monomials *)

  val add : t -> t -> t
  (** returns the addition of two polynomials *)

  val mult : t -> t -> t
  (** returns the multiplication of two polynomials *)

  val euclidian_div : t -> t -> t * t
  (** returns (quotient, remainder) of the euclidian division of two polynomials *)

  val multi_euclidian_div : t -> t list -> t list * t
  (** returns (quotients, remainder) of the euclidien division of a polynomials by a list of well-ordered polynomials *)

  val ( *: ) : Q.t -> Mono.t -> t
  val ( *& ) : Q.t -> t -> t
  val ( *@ ) : Mono.t -> t -> t
  val ( *@& ) : cm -> t -> t
  val ( *> ) : t -> t -> t
  val ( + ) : t -> t -> t
  val ( - ) : t -> t -> t
  val ( ~- ) : t -> t
  val sort : t list -> t list
  val pp : Format.formatter -> t -> unit
end

module MkPolynomial (Mono : Monomial.OrderedMonomial) :
  Poly with type Mono.t = Mono.t
