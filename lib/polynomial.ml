module type Poly = sig
  module Mono : Monomial.OrderedMonomial

  type cm = { coeff : Q.t; mdeg : Mono.t }
  type t (* DO NOT EXPOSE *)

  val of_mono : Q.t -> Mono.t -> t
  val zero : t
  val is_zero : t -> bool
  val eq : t -> t -> bool
  val lt : t -> cm
  val lc : t -> Q.t
  val lm : t -> Mono.t
  val prod_coef : Q.t -> t -> t
  val mult_mono : Mono.t -> t -> t
  val mult_coef_mono : cm -> t -> t
  val add : t -> t -> t
  val mult : t -> t -> t
  val euclidian_div : t -> t -> t * t
  val multi_euclidian_div : t -> t list -> t list * t
  val ( *: ) : Q.t -> Mono.t -> t
  val ( *& ) : Q.t -> t -> t
  val ( *@ ) : Mono.t -> t -> t
  val ( *@& ) : cm -> t -> t
  val ( *> ) : t -> t -> t
  val ( + ) : t -> t -> t
  val ( - ) : t -> t -> t
  val ( ~- ) : t -> t
  val sort : t list -> t list
  val pp : Format.formatter -> t -> unit
end

module MkPolynomial (Mono : Monomial.OrderedMonomial) :
  Poly with type Mono.t = Mono.t = struct
  module Mono = Mono

  type cm = { coeff : Q.t; mdeg : Mono.t }
  type t = cm list

  let of_mono (coeff : Q.t) (mdeg : Mono.t) : t = [ { coeff; mdeg } ]
  let zero = of_mono Q.zero Mono.cst

  let is_zero = function
    | [] -> true
    | [ { coeff; _ } ] -> Q.equal coeff Q.zero
    | _ -> false

  let normalize t =
    match t with
    | [] -> zero
    | t -> List.filter (fun x -> not (Q.equal x.coeff Q.zero)) t

  let eq p1 p2 =
    List.for_all2
      (fun x y -> Q.equal x.coeff y.coeff && Mono.eq x.mdeg y.mdeg)
      p1 p2

  let pp_cm fmt cm =
    if Mono.total_deg cm.mdeg == 0 then
      Format.fprintf fmt "%a" Q.pp_print cm.coeff
    else if Q.equal cm.coeff Q.one then Format.fprintf fmt "%a" Mono.pp cm.mdeg
    else if Q.equal cm.coeff Q.minus_one then
      Format.fprintf fmt "- %a" Mono.pp cm.mdeg
    else if Q.(cm.coeff < zero) then
      Format.fprintf fmt "- %a%a" Q.pp_print (Q.abs cm.coeff) Mono.pp cm.mdeg
    else Format.fprintf fmt "%a%a" Q.pp_print cm.coeff Mono.pp cm.mdeg

  let pp fmt t =
    let aux i x =
      if Q.(x.coeff < zero) then
        Format.fprintf fmt "%s%a" (if i != 0 then " " else "") pp_cm x
      else Format.fprintf fmt "%s%a" (if i != 0 then " + " else "") pp_cm x
    in
    Format.fprintf fmt "[";
    List.iteri aux t;
    Format.fprintf fmt "]"

  let lt t = List.hd t
  let lc t = (lt t).coeff
  let lm t = (lt t).mdeg

  let prod_coef (c : Q.t) (p : t) : t =
    List.map (fun { coeff; mdeg } -> { coeff = Q.mul c coeff; mdeg }) p
    |> normalize

  let mult_mono (mon : Mono.t) (p : t) : t =
    List.map (fun { coeff; mdeg } -> { coeff; mdeg = Mono.mult mdeg mon }) p
    |> normalize

  let mult_coef_mono { coeff; mdeg } (p : t) : t =
    List.map
      (fun { coeff = coeff2; mdeg = mdeg2 } ->
        { coeff = Q.mul coeff coeff2; mdeg = Mono.mult mdeg mdeg2 })
      p
    |> normalize

  let add (p1 : t) (p2 : t) : t =
    let rec merge acc q1 q2 : t =
      match (q1, q2) with
      | [], [] -> List.rev acc
      | { coeff = c; _ } :: t1, q2 when Q.equal c Q.zero -> merge acc t1 q2
      | q1, { coeff = c; _ } :: t2 when Q.equal c Q.zero -> merge acc q1 t2
      | [], x :: t | x :: t, [] -> merge (x :: acc) t []
      | ( ({ coeff = c1; mdeg = m1 } as x1) :: t1,
          ({ coeff = c2; mdeg = m2 } as x2) :: t2 ) ->
          let cmp = Mono.compare m1 m2 in
          if cmp > 0 then merge (x1 :: acc) t1 q2
          else if cmp < 0 then merge (x2 :: acc) t2 q1
          else if Q.(c1 + c2 == zero) then merge acc t1 t2
          else merge ({ coeff = Q.add c1 c2; mdeg = m1 } :: acc) t1 t2
    in
    merge [] p1 p2 |> normalize

  let neg (t : t) : t =
    List.map (fun { coeff; mdeg } -> { coeff = Q.neg coeff; mdeg }) t

  let sub t1 t2 = add t1 (neg t2)

  let mult (p1 : t) (p2 : t) : t =
    List.fold_left (fun acc m -> add acc (mult_coef_mono m p2)) [] p1
    |> normalize

  let euclidian_div (f : t) (g : t) =
    let loop_div (p : t) (q : t) =
      match Mono.div_eucl (lm p) (lm g) with
      | None -> (p, q, false)
      | Some quotient_lt ->
          (* Printf.printf "new round:\n"; *)
          let tmp = of_mono (Q.div (lc p) (lc g)) quotient_lt in
          (* Printf.printf "tmp = %a \n" Mono.pp_monomial tmp; *)
          let q_neg = neg tmp in
          (* Printf.printf "q_neg = %a\n" Mono.pp_monomial q_neg; *)
          let q = add q tmp in
          (* Printf.printf "q = %a\n" pp q;
             Printf.printf "g = %a\n" pp g; *)
          let p = add p (mult g q_neg) in
          (p, q, true)
    in
    let rec loop_rem p q r =
      (* Printf.printf "loop_rem : %a %a %a\n%!" pp p pp q pp r; *)
      if is_zero p then (q, r)
      else
        let p, q, div_occurred = loop_div p q in
        if div_occurred then loop_rem p q r
        else
          let leading_term = [ lt p ] in
          let leading_term_neg = neg leading_term in
          let r = add r leading_term in
          let p = add p leading_term_neg in
          loop_rem p q r
    in
    loop_rem f zero zero

  let multi_euclidian_div (f : t) (sf : t list) =
    let n = List.length sf in
    let sfi = List.mapi (fun i x -> (i, x)) sf in
    let qs = Array.init n (fun _ -> zero) in
    let rec loop (p : t) (r : t) =
      let rec loopi (s : (int * t) list) (p : t) =
        match s with
        | [] -> None
        | (i, fi) :: t -> (
            match Mono.div_eucl (lm p) (lm fi) with
            | None -> loopi t p
            | Some m ->
                let q = of_mono (Q.div (lc p) (lc fi)) m in
                qs.(i) <- add qs.(i) q;
                Some (sub p (mult q fi)))
      in
      if is_zero p then r
      else
        match loopi sfi p with
        | None ->
            let ltp = [ lt p ] in
            let r' = add r ltp in
            let p' = sub p ltp in
            loop p' r'
        | Some p' -> loop p' r
    in
    let r = loop f zero in
    (Array.to_list qs, r)

  let sort (l : t list) = List.sort (fun p q -> Mono.compare (lm p) (lm q)) l
  let ( *: ) = of_mono
  let ( *@ ) = mult_mono
  let ( *& ) = prod_coef
  let ( *@& ) = mult_coef_mono
  let ( *> ) = mult
  let ( + ) = add
  let ( - ) = sub
  let ( ~- ) = neg
end

(*Todo:
  - tester les exemples du boouquin...
  - tester les fonctions avec qq polynomes
  - buchberger ameliore p114
  - liste des mdeg affichés a chaque etape : FAIT
  - separer buchberger pour plus de lisibilité : FAIT
  - Se passer de Base: pour le moment compliqué, il faut coder un cartesian product. FAIT
  - comment les recuperer : stocker dans un .txt la liste des polynomes d'origine + les mdeg etapes par etape: Pour le moment sur terminal
  - un fichier .txt avec : Les exemples et pour chaque etape de buchberger la liste des mdeg pour verifier qu'ils explosent pas.
*)
