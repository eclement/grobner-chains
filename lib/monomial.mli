module type DimMonomial = sig
  val dim : int
  val var_name : string array

  type t = private int array

  val make : int array -> t
  val one_var : int -> t
  val cst : t

  val deg : t -> int -> int
  (** returns the degree associated to a given variable *)

  val mult : t -> t -> t
  (** returns the multiplication of two monomials *)

  val power : t -> int -> t
  (** returns the monomial powered by an unsigned integer *)

  val lcm : t -> t -> t
  (** returns the maximum (coefficient by coefficient) of two mdeg *)

  val total_deg : t -> int
  (** returns the sum of coefficients of the mdeg *)

  val eq : t -> t -> bool
  (** verifies the equality of two monomials *)

  val div_eucl : t -> t -> t option
  (** verifies if a monomial m1 divides another one m2, and return (Some quotient) if so, and None otherwise, where quotient = m1/m2 *)

  val pp : Format.formatter -> t -> unit
  val ( * ) : t -> t -> t
  val ( ** ) : t -> int -> t
  val ( /? ) : t -> t -> t option
  val ( = ) : t -> t -> bool
end

module MkMonomial (Dim : sig
  val dim : int
  val var_name : string array
end) : DimMonomial

module type OrderedMonomial = sig
  include DimMonomial

  val compare : t -> t -> int
end

module MkLexOrderedMonomial (M : DimMonomial) :
  OrderedMonomial with type t = M.t

module MkGradLexOrderedMonomial (M : DimMonomial) :
  OrderedMonomial with type t = M.t
