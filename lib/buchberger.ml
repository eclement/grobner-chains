let cartesian_product (l1 : 'a list) (l2 : 'b list) : ('a * 'b) list =
  let rec loop acc xs1 xs2 =
    match (xs1, xs2) with
    | [], _ -> acc
    | _ :: t1, [] -> loop acc t1 l2
    | x1 :: _, x2 :: t2 -> loop ((x1, x2) :: acc) xs1 t2
  in
  match (l1, l2) with [], _ | _, [] -> [] | _, _ -> loop [] l1 l2 |> List.rev

module type Buchberger = sig
  module P : Polynomial.Poly

  val s_poly : P.t -> P.t -> P.t
  val buchberger : P.t list -> P.t list
  val optimized_buchberger : P.t list -> P.t list
end

module MkBuchberger (P : Polynomial.Poly) : Buchberger with type P.t = P.t =
struct
  module P = P

  let s_poly (f : P.t) (g : P.t) =
    let open P in
    let alpha = P.lm f and beta = P.lm g in
    let div_left = Mono.lcm alpha beta in
    (* Q.one *: *)
    let gamma_left = Option.get (Mono.div_eucl div_left (P.lm f)) in
    let gamma_left = Q.div Q.one (P.lc f) *: gamma_left in
    let gamma_right =
      Option.get (Mono.div_eucl (Mono.lcm alpha beta) (P.lm g))
    in
    let gamma_right = Q.div Q.minus_one (P.lc g) *: gamma_right in
    let s_left = gamma_left *> f in
    let s_right = gamma_right *> g in
    s_left + s_right

  let buchberger (t_list : P.t list) : P.t list =
    (* Warning: order of the added buchberger base is not as expected: the first r to be added is left-added, then the next one is also left-added... In the algorithm, all are right-added *)
    (* pl : list of the indexed polynomials *)
    (* wl: worklist of the polynomials of G f_i, f_j such that i < j *)
    let t_list_gen = List.map (fun x -> (x, 0)) t_list in
    let pl = List.mapi (fun i x -> (i, x)) t_list_gen in
    let wl =
      List.concat_map
        (fun (i, x) ->
          List.filter_map
            (fun (j, y) -> if j <= i then None else Some (x, y))
            pl)
        pl
    in
    let rec loop (g' : (P.t * int) list) = function
      | [] -> g' (* If wl empty, return g': g = g' *)
      | ((p, i), (q, j)) :: t ->
          (* there is an element (p,q) in the wl to test *)
          let g_no_gen = List.map fst g' in
          let _, r = P.multi_euclidian_div (s_poly p q) g_no_gen in
          if P.is_zero r then loop g' t (* r = 0, pass to the next candidate *)
          else
            (* r != 0, let's add it to the g and increase the wl *)
            let gen = max i j + 1 in
            Format.printf "Add r = %a (generation = %d)\n%!" P.pp r gen;
            loop ((r, gen) :: g')
              (List.fold_left (fun acc x -> (x, (r, gen)) :: acc) t g')
      (* add r to the g' and { (f_i, r) ; f_i in g' } to the wl to test them *)
    in
    Format.printf "Buchberger start\n%!";
    List.iter (Format.printf "%a\n%!" P.pp) t_list;
    Format.printf "Buchberger end args\n%!";
    loop t_list_gen wl |> List.map (fun (x, _) -> x)

  let _buchberger_degree (ts : P.t list) = buchberger ts |> List.map P.lm

  let buchberger_criterion (i : int) (j : int) (t_list : P.t array)
      (b : (int * int) list) (s : int) : bool =
    (*
       - (i,j) are the position of f_i and f_j in the initial list F = < f_0 , ... , f_(s-1) >
       - t_list is the list of polynomial < f_0, ... , f_(s-1) >
       - b is the list of remaining (i,j) that is not supressed by the while loop
       - s is the initial number of polynomials in G
    *)
    (* Pour tout (i,l), (j,l) in [0...(s-1)]**2, check if (i,l) and (j,l) are NOT in b, if so, check LT(f_l) divide lcm(LT(f_i), LT(f_j))
       If theses two conditions are true, return true, otherwise, return false *)
    let lcm_i_j = P.Mono.lcm (P.lm t_list.(i)) (P.lm t_list.(j)) in
    let f (k : int) : bool =
      match
        List.exists (fun (a, b) -> (a = i && b = k) || (a = k && b = j)) b
      with
      | true -> false
      | false -> Option.is_some (P.Mono.div_eucl (P.lm t_list.(k)) lcm_i_j)
    in
    List.exists (fun k -> f k) (List.init s Fun.id)

  let optimized_buchberger (t_list : P.t list) =
    let t_list = List.map (fun x -> (x, 0)) t_list in
    let t_array = Array.of_list t_list in
    let l = List.init (Array.length t_array) Fun.id in
    (* worklist creation *)
    let b = cartesian_product l l |> List.filter (fun (i, j) -> i < j) in

    let rec loop (worklist : (int * int) list) (g : (P.t * int) array) :
        (P.t * int) list =
      match worklist with
      | [] -> Array.to_list g (* base case *)
      | (i, j) :: wl ->
          let s = Array.length g in
          let f_i, gen_i = g.(i) in
          let f_j, gen_j = g.(j) in
          let g_no_gen = Array.map fst g in
          let lcm_i_j = P.Mono.lcm (P.lm f_i) (P.lm f_j) in
          let b_0 = not (P.Mono.eq (P.lm (P.mult f_i f_j)) lcm_i_j) in
          if b_0 && not (buchberger_criterion i j g_no_gen worklist s) then (
            let _, r =
              P.multi_euclidian_div (s_poly f_i f_j) (Array.to_list g_no_gen)
            in
            if P.is_zero r then loop wl g
            else
              let gen = 1 + max gen_i gen_j in
              Format.printf
                "Add opt r = %a (gen %d)\n\
                 from p = %a (gen %d)\n\
                 and q = %a (gen %d)\n\
                 %!"
                P.pp r gen P.pp f_i gen_i P.pp f_j gen_j;
              Format.printf "with worklist = %a\n%!"
                (fun fmt l ->
                  Format.fprintf fmt "[";
                  List.iter (fun (x, y) -> Format.fprintf fmt "(%d,%d); " x y) l;
                  Format.fprintf fmt "]")
                worklist;
              let wl = List.init s (fun x -> (x, s)) @ wl in
              let g = Array.append g [| (r, gen) |] in
              loop wl g)
          else loop wl g
    in
    loop b t_array |> List.map (fun (x, _) -> x)
end
