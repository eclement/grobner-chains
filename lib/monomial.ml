module type DimMonomial = sig
  val dim : int
  val var_name : string array

  type t = private int array

  val make : int array -> t
  val one_var : int -> t
  val cst : t
  val deg : t -> int -> int
  val mult : t -> t -> t
  val power : t -> int -> t
  val lcm : t -> t -> t
  val total_deg : t -> int
  val eq : t -> t -> bool
  val div_eucl : t -> t -> t option
  val pp : Format.formatter -> t -> unit
  val ( * ) : t -> t -> t
  val ( ** ) : t -> int -> t
  val ( /? ) : t -> t -> t option
  val ( = ) : t -> t -> bool
end

module MkMonomial (Dim : sig
  val dim : int
  val var_name : string array
end) : DimMonomial = struct
  assert (Array.length Dim.var_name == Dim.dim)

  let dim = Dim.dim
  let var_name = Dim.var_name

  type t = int array

  let make arr : t =
    assert (Array.length arr == dim);
    Array.copy arr

  let one_var (x : int) : t =
    assert (x < dim);
    Array.init dim (fun i -> if i == x then 1 else 0)

  let cst : t = Array.init dim (fun _ -> 0)

  let deg m i =
    assert (i < dim);
    m.(i)

  let mult (m1 : t) (m2 : t) : t = Array.map2 ( + ) m1 m2
  let power (m : t) (x : int) : t = Array.map (fun y -> y * x) m
  let lcm m1 m2 = Array.map2 max m1 m2
  let total_deg (m : t) = Array.fold_left ( + ) 0 m
  let eq m1 m2 = Array.for_all2 ( = ) m1 m2

  let div_eucl (m1 : t) (m2 : t) : t option =
    let quotient = Array.map2 ( - ) m1 m2 in
    (*Diff vector: Substract the two degrees*)
    if
      Array.for_all (fun x -> x >= 0) quotient
      (*Check the positivity of the difference vector*)
    then Some quotient
    else None

  let pp_var name fmt d =
    if d > 1 then Format.fprintf fmt "%s^%d" name d
    else if d == 1 then Format.fprintf fmt "%s" name
    else ()

  let pp fmt (m : t) =
    Array.iter2
      (fun d name -> Format.fprintf fmt "%a" (pp_var name) d)
      m var_name

  let ( * ) = mult
  let ( ** ) = power
  let ( /? ) = div_eucl
  let ( = ) = eq
end

module type OrderedMonomial = sig
  include DimMonomial

  val compare : t -> t -> int
end

module MkLexOrderedMonomial (M : DimMonomial) :
  OrderedMonomial with type t = M.t = struct
  include M

  let compare (t1 : t) (t2 : t) =
    let rec loop i =
      if i >= dim then 0
      else match deg t1 i - deg t2 i with 0 -> loop (i + 1) | n -> n
    in
    loop 0
end

module MkGradLexOrderedMonomial (M : DimMonomial) :
  OrderedMonomial with type t = M.t = struct
  include M

  let compare (t1 : t) (t2 : t) =
    let rec loop i m s =
      if i >= dim then if s == 0 then m else s
      else
        let sub = deg t1 i - deg t2 i in
        loop (i + 1) (if m == 0 then sub else m) (s + sub)
    in
    loop 0 0 0
end
