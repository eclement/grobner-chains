module type Buchberger = sig
  module P : Polynomial.Poly

  val s_poly : P.t -> P.t -> P.t
  val buchberger : P.t list -> P.t list
  val optimized_buchberger : P.t list -> P.t list
end

module MkBuchberger (P : Polynomial.Poly) : Buchberger with type P.t = P.t
