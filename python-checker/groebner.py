"""
Simple Script, using Sympy, to check Buchberger's hand-made computations.

The possible computations are:
 - optimised Buchberger
 - F5

 Both specifying the order (lex, grlex), and specifying the order of the x,y,z variables...

 Warning: it computes the reduced Groebner Basis. So it can supress useless polynomials at the end.
"""
from sympy import poly, expand, lcm, LM, LT, reduced, monic
from sympy import degree
from sympy import Poly, S, QQ
from sympy.abc import x, y, z
from sympy import groebner, symmetric_poly


def s_polynomial(f, g, order):
    return expand(
        lcm(
            LM(f, order=order),
            LM(g, order=order)) * (1 / LT(f, order=order) * f - 1 / LT(g, order=order) * g),
        order=order
    )


def test_buch_lex(p_list, gens=None):
    """
    WARNING: check order of gens
    :param p_list:
    :return:
    """
    F = [Poly(p, domain='QQ') for p in p_list]
    if gens is None:
        gens = list({x for f in F for x in f.gens})
    print(gens)
    return groebner(F, *gens, order='lex', method='buchberger')


def test_buch_grlex(p_list, gens=None):
    F = [Poly(p, domain='QQ') for p in p_list]
    if gens is None:
        gens = list({x for f in F for x in f.gens})
    print(gens)
    return groebner(F, *gens, order='grlex', method='buchberger')


def test_f5b_lex(p_list, gens=None):
    F = [Poly(p, domain='QQ') for p in p_list]
    if gens is None:
        gens = list({x for f in F for x in f.gens})
    print(gens)
    return groebner(F, *gens, order='grlex', method='f5b')


def test_f5b_grlex(p_list, gens):
    F = [Poly(p, domain='QQ') for p in p_list]
    if gens is None:
        gens = list({x for f in F for x in f.gens})
    print(gens)
    return groebner(F, *gens, order='grlex', method='f5b')
