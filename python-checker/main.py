"""
Simple Script, using Sympy, to check Buchberger's hand-made computations.

The possible computations are:
 - optimised Buchberger
 - F5

 Both specifying the order (lex, grlex), and specifying the order of the x,y,z variables...

 Warning: it computes the reduced Groebner Basis. So it can supress useless polynomials at the end.
"""
from sympy import poly, expand, lcm, LM, LT, reduced, monic
from sympy import degree
from sympy import Poly, S, QQ
from sympy.abc import x, y, z, t
from sympy import groebner
from groebner import *

def ex_1_two_var():
    """
    Example for Cox book
    """
    F = [x ** 3 - 2 * x * y, (x ** 2) * y - 2 * (y ** 2) + x]
    G = [x ** 3 - 2 * x * y, (x ** 2) * y - 2 * (y ** 2) + x, x ** 2, x - 2 * y ** 2, y ** 3]
    f1 = F[0]
    f2 = F[1]
    f3 = - x ** 2
    f4 = -2 * x * y
    f5 = -2 * (y ** 2) + x
    F_1 = [f1, f2, f3]
    F_2 = [f1, f2, f3, f4]
    s_f1_f2 = s_polynomial(f1, f2, 'lex')
    s_f1_f3 = s_polynomial(f1, f3, 'lex')
    s_f1_f4 = s_polynomial(f1, f4, 'lex')
    s_f2_f3 = s_polynomial(f2, f3, 'lex')
    print("f1_f2 check")
    print(reduced(s_f1_f2, F, order='lex'))
    print("f1_f3 check")
    print(reduced(s_f1_f3, F_1, order='lex'))
    print("f1_f4 check")
    print(reduced(s_f1_f4, F_2, order='lex'))
    print("f2_f3 check")
    print(reduced(s_f2_f3, F_2, order='lex'))
    print("lex buchberger for F then G")
    print(test_buch_lex(G, [x, y]))
    print(test_buch_lex(F, [x, y]))
    print("grlex buchberger for F then G")
    print(test_buch_grlex(G, [x, y]))
    print(test_buch_grlex(F, [x, y]))

def ex_2_three_var():
    F2 = [x + z, y - z]
    print(test_buch_grlex(F2, [x, y, z]))

def ex_3_three_var():
    F = [ x**3 - 2*x*y + z**3 + x*y, (x**2)*y - 2*(y**2) + z, y*(z**2) - (z**2)]
    f1, f2, f3 = F[0], F[1], F[2]
    print("f1 leading monomial:")
    print(LM(f1, order='lex'))
    print("f2 leading monomial:")
    print(LM(f2, order='lex'))
    print("f3 leading monomial:")
    print(LM(f3, order='lex'))
    print("G:")
    print(F)
    print("s_poly of the polynomials f1, f2")
    s_f1_f2 = s_polynomial(f1, f2, 'lex')
    print(s_f1_f2)
    print("Euclidian division of s_f1_f2 with polynomials of F:")
    print(reduced(s_f1_f2, F, order='lex'))
    print("Let us add f4 to G:")
    f4 = x*(y**2) - x*z + z**3
    print("new G:")
    G = [f1, f2, f3, f4]
    print("s_poly of the polynomials f1 f3")
    s_f1_f3 = s_polynomial(f1, f3, 'lex')
    print(s_f1_f3)
    print("Euclidian division of s_f1_f3 with polynomials of G:")
    print(reduced(s_f1_f3, G, order='lex'))
    print("Do not add, let us check (1,4)")
    print("s_poly of the polynomials f1 f4")
    s_f1_f4 = s_polynomial(f1, f4, 'lex')
    print(s_f1_f4)
    print("Euclidian division of s_f1_f4 with polynomials of G:")
    print(reduced(s_f1_f4, G, order='lex'))
    print("Let us add polynomial f5:")
    f5 = -x**2*z**3 - z**4 + 2*z**3
    print(f5)
    G.append(f5)
    pairs = [(1,5), (2,3), (2,4), (2,5), (3,4), (3,5), (4,5)]
    print("New pairs")
    print(pairs)
    print("s_poly of the polynomials f1 f5")
    s_f1_f5 = s_polynomial(f1, f5, 'lex')
    print(s_f1_f5)
    print("Euclidian division of s_f1_f5 with polynomials of G:")
    print(reduced(s_f1_f5, G, order='lex'))
    print("Add f6:")
    f6=-x*z**4 + x*z**3 + z**6
    G.append(f6)
    pairs = [(1, 6), (2, 3), (2, 4), (2, 5), (2,6), (3, 4), (3, 5), (3,6), (4, 5), (4,6) , (5,6)]
    print("New pairs")
    print(pairs)
    print("s_poly of the polynomials f1 f6")
    s_f1_f6 = s_polynomial(f1, f6, 'lex')
    print(s_f1_f6)
    print("Euclidian division of s_f1_f6 with polynomials of G:")
    print(reduced(s_f1_f6, G, order='lex'))

    print("s_poly of the polynomials f2 f3")
    s_f2_f3 = s_polynomial(f2, f3, 'lex')
    print(s_f2_f3)
    print("Euclidian division of s_f2_f3 with polynomials of G:")
    print(reduced(s_f2_f3, G, order='lex'))
    f7 = x**2*z**2 + z**3 - 2*z**2
    print("add f7 to G:")
    G.append(f7)
    print(f7)
    pairs = [(1,7), (2, 4), (2, 5), (2, 6), (2,7), (3, 4), (3, 5), (3, 6), (3,7), (4, 5), (4, 6), (4,7), (5, 6), (5,7),
             (6,7)]
    print("New pairs")
    print(pairs)
    print("s_poly of the polynomials f1 f7")
    s_f1_f7 = s_polynomial(f1, f7, 'lex')
    print(s_f1_f7)
    print("Euclidian division of s_f1_f7 with polynomials of G:")
    print(reduced(s_f1_f7, G, order='lex'))
    f8 = -x*z**3 + x*z**2 + z**5
    print("Add f8:")
    print(f8)
    G.append(f8)
    pairs = [(1,8),
             (2, 4), (2, 5), (2, 6), (2, 7), (2,8),
             (3, 4), (3, 5), (3, 6), (3, 7), (3,8),
             (4, 5), (4, 6), (4, 7), (4,8),
             (5, 6), (5, 7), (5,8),
             (6, 7), (6,8),
             (7,8)]
    print("New pairs:")
    print(pairs)
    print("s_poly of the polynomials f1 f8")
    s_f1_f8 = s_polynomial(f1, f8, 'lex')
    print(s_f1_f8)
    print("Euclidian division of s_f1_f8 with polynomials of G:")
    print(reduced(s_f1_f8, G, order='lex'))
    pairs = [(2, 4), (2, 5), (2, 6), (2, 7), (2, 8),
             (3, 4), (3, 5), (3, 6), (3, 7), (3, 8),
             (4, 5), (4, 6), (4, 7), (4, 8),
             (5, 6), (5, 7), (5, 8),
             (6, 7), (6, 8),
             (7, 8)]
    print("s_poly of the polynomials f2 f4")
    s_f2_f4 = s_polynomial(f2, f4, 'lex')
    print(s_f2_f4)
    print("Euclidian division of s_f2_f4 with polynomials of G:")
    print(reduced(s_f2_f4, G, order='lex'))
    f9 = x**2*z - x*z**2 - 2*y**3 + y*z - z**5
    G.append(f9)
    pairs = [(2, 5), (2, 6), (2, 7), (2, 8), (2, 9),
             (3, 4), (3, 5), (3, 6), (3, 7), (3, 8), (3, 9),
             (4, 5), (4, 6), (4, 7), (4, 8), (4, 9),
             (5, 6), (5, 7), (5, 8), (5, 9),
             (6, 7), (6, 8), (6, 9),
             (7, 8), (7, 9),
             (8, 9)]
    print("new pairs:")
    print(pairs)
    print("s_poly of the polynomials f2 f5")
    s_f2_f5 = s_polynomial(f2, f5, 'lex')
    print(s_f2_f5)
    print("Euclidian division of s_f2_f5 with polynomials of G:")
    print(reduced(s_f2_f5, G, order='lex'))
    print("s_poly of the polynomials f2 f6")
    s_f2_f6 = s_polynomial(f2, f6, 'lex')
    print(s_f2_f6)
    print("Euclidian division of s_f2_f6 with polynomials of G:")
    print(reduced(s_f2_f6, G, order='lex'))
    f_1_0 = x*z**2 + z**8 + z**7 + z**6 + 2*z**5 - 3*z**4 + 2*z**3
    G.append(f_1_0)
    pairs = [(2, 7), (2, 8), (2, 9), (2, 10),
             (3, 4), (3, 5), (3, 6), (3, 7), (3, 8), (3, 9), (3, 10),
             (4, 5), (4, 6), (4, 7), (4, 8), (4, 9), (4, 10),
             (5, 6), (5, 7), (5, 8), (5, 9), (5, 10),
             (6, 7), (6, 8), (6, 9), (6, 10),
             (7, 8), (7, 9), (7, 10),
             (8, 9), (8, 10),
             (9, 10)]
    print(pairs)
    print("s_poly of the polynomials f2 f7")
    s_f2_f7 = s_polynomial(f2, f7, 'lex')
    print(s_f2_f7)
    print("Euclidian division of s_f2_f7 with polynomials of G:")
    print(reduced(s_f2_f7, G, order='lex'))
    print("s_poly of the polynomials f2 f8")
    s_f2_f8 = s_polynomial(f2, f8, 'lex')
    print(s_f2_f8)
    print("Euclidian division of s_f2_f8 with polynomials of G:")
    print(reduced(s_f2_f8, G, order='lex'))
    f_1_1 = -z**8 - z**5 + 4*z**4 - 5*z**3 + 2*z**2
    G.append(f_1_1)
    pairs = [(2, 9), (2, 10), (2, 11),
             (3, 4), (3, 5), (3, 6), (3, 7), (3, 8), (3, 9), (3, 10), (3, 11),
             (4, 5), (4, 6), (4, 7), (4, 8), (4, 9), (4, 10), (4, 11),
             (5, 6), (5, 7), (5, 8), (5, 9), (5, 10), (5, 11),
             (6, 7), (6, 8), (6, 9), (6, 10), (6, 11),
             (7, 8), (7, 9), (7, 10), (7, 11),
             (8, 9), (8, 10), (8, 11),
             (9, 10), (9 ,11),
             (10, 11)]
    print("s_poly of the polynomials f2 f9")
    s_f2_f9 = s_polynomial(f2, f9, 'lex')
    print(s_f2_f9)
    print("Euclidian division of s_f2_f9 with polynomials of G:")
    print(reduced(s_f2_f9, G, order='lex'))
    f_1_2 = 2*y**4 - 3*y**2*z - z**7 - z**6 - z**4 + 3*z**3 - z**2
    G.append(f_1_2)
    pairs = [(2, 10), (2, 11), (2, 12),
             (3, 4), (3, 5), (3, 6), (3, 7), (3, 8), (3, 9), (3, 10), (3, 11), (3, 12),
             (4, 5), (4, 6), (4, 7), (4, 8), (4, 9), (4, 10), (4, 11), (4, 12),
             (5, 6), (5, 7), (5, 8), (5, 9), (5, 10), (5, 11), (5, 12),
             (6, 7), (6, 8), (6, 9), (6, 10), (6, 11), (6, 12),
             (7, 8), (7, 9), (7, 10), (7, 11), (7, 12),
             (8, 9), (8, 10), (8, 11), (8, 12),
             (9, 10), (9, 11), (9, 12),
             (10, 11), (10, 12),
             (11, 12)]
    print("s_poly of the polynomials f2 f10")
    s_f2_f_1_0 = s_polynomial(f2, f_1_0, 'lex')
    print(s_f2_f_1_0)
    print("Euclidian division of s_f2_f10 with polynomials of G:")
    print(reduced(s_f2_f_1_0, G, order='lex'))
    print("s_poly of the polynomials f2 f111")
    s_f2_f_1_1 = s_polynomial(f2, f_1_1, 'lex')
    print(s_f2_f_1_1)
    print("Euclidian division of s_f2_f11 with polynomials of G:")
    print(reduced(s_f2_f_1_1, G, order='lex'))
    print("s_poly of the polynomials f2 f12")
    s_f2_f_1_2 = s_polynomial(f2, f_1_2, 'lex')
    print(s_f2_f_1_2)
    print("Euclidian division of s_f2_f12 with polynomials of G:")
    print(reduced(s_f2_f_1_2, G, order='lex'))
    f_1_3 = y**3*z - z**7/2 - z**4/2 + 2*z**3 - 5*z**2/2
    G.append(f_1_3)
    pairs = [(2, 13),
             (3, 4), (3, 5), (3, 6), (3, 7), (3, 8), (3, 9), (3, 10), (3, 11), (3, 12), (3, 13),
             (4, 5), (4, 6), (4, 7), (4, 8), (4, 9), (4, 10), (4, 11), (4, 12), (4, 13),
             (5, 6), (5, 7), (5, 8), (5, 9), (5, 10), (5, 11), (5, 12), (5, 13),
             (6, 7), (6, 8), (6, 9), (6, 10), (6, 11), (6, 12), (6, 13),
             (7, 8), (7, 9), (7, 10), (7, 11), (7, 12), (7, 13),
             (8, 9), (8, 10), (8, 11), (8, 12), (8, 13),
             (9, 10), (9, 11), (9, 12), (9, 13),
             (10, 11), (10, 12), (10, 13),
             (11, 12), (11, 13),
             (12, 13)]
    print(pairs)
    print("s_poly of the polynomials f2 f13")
    s_f2_f_1_3 = s_polynomial(f2, f_1_3, 'lex')
    print(s_f2_f_1_3)
    print("Euclidian division of s_f2_f13 with polynomials of G:")
    print(reduced(s_f2_f_1_3, G, order='lex'))

    print("s_poly of the polynomials f3 f4")
    s_f3_f4 = s_polynomial(f3, f4, 'lex')
    print(s_f3_f4)
    print("Euclidian division of s_f3_f4 with polynomials of G:")
    print(reduced(s_f3_f4, G, order='lex'))

    print("s_poly of the polynomials f3 f5")
    s_f3_f5 = s_polynomial(f3, f5, 'lex')
    print(s_f3_f5)
    print("Euclidian division of s_f3_f5 with polynomials of G:")
    print(reduced(s_f3_f5, G, order='lex'))

    print("s_poly of the polynomials f3 f6")
    s_f3_f6 = s_polynomial(f3, f6, 'lex')
    print(s_f3_f6)
    print("Euclidian division of s_f3_f6 with polynomials of G:")
    print(reduced(s_f3_f6, G, order='lex'))

    print("s_poly of the polynomials f3 f7")
    s_f3_f7 = s_polynomial(f3, f7, 'lex')
    print(s_f3_f7)
    print("Euclidian division of s_f3_f7 with polynomials of G:")
    print(reduced(s_f3_f7, G, order='lex'))

    print("s_poly of the polynomials f3 f8")
    s_f3_f8 = s_polynomial(f3, f8, 'lex')
    print(s_f3_f8)
    print("Euclidian division of s_f3_f8 with polynomials of G:")
    print(reduced(s_f3_f8, G, order='lex'))

    print("s_poly of the polynomials f3 f9")
    s_f3_f9 = s_polynomial(f3, f9, 'lex')
    print(s_f3_f9)
    print("Euclidian division of s_f3_f9 with polynomials of G:")
    print(reduced(s_f3_f9, G, order='lex'))

    print("s_poly of the polynomials f3 f_1_0")
    s_f3_f_1_0 = s_polynomial(f3, f_1_0, 'lex')
    print(s_f3_f_1_0)
    print("Euclidian division of s_f3_f_1_0 with polynomials of G:")
    print(reduced(s_f3_f_1_0, G, order='lex'))

    print("s_poly of the polynomials f3 f_1_1")
    s_f3_f_1_1 = s_polynomial(f3, f_1_1, 'lex')
    print(s_f3_f_1_1)
    print("Euclidian division of s_f3_f_1_1 with polynomials of G:")
    print(reduced(s_f3_f_1_1, G, order='lex'))

    print("s_poly of the polynomials f3 f_1_2")
    s_f3_f_1_2 = s_polynomial(f3, f_1_2, 'lex')
    print(s_f3_f_1_2)
    print("Euclidian division of s_f3_f_1_2 with polynomials of G:")
    print(reduced(s_f3_f_1_2, G, order='lex'))

    print("s_poly of the polynomials f3 f_1_3")
    s_f3_f_1_3 = s_polynomial(f3, f_1_3, 'lex')
    print(s_f3_f_1_3)
    print("Euclidian division of s_f3_f_1_3 with polynomials of G:")
    print(reduced(s_f3_f_1_3, G, order='lex'))

    print("s_poly of the polynomials f4 f5")
    s_f4_f5 = s_polynomial(f4, f5, 'lex')
    print(s_f4_f5)
    print("Euclidian division of s_f4_f5 with polynomials of G:")
    print(reduced(s_f4_f5, G, order='lex'))

    print("s_poly of the polynomials f4 f6")
    s_f4_f6 = s_polynomial(f4, f6, 'lex')
    print(s_f4_f6)
    print("Euclidian division of s_f4_f6 with polynomials of G:")
    print(reduced(s_f4_f6, G, order='lex'))

    print("s_poly of the polynomials f4 f7")
    s_f4_f7 = s_polynomial(f4, f7, 'lex')
    print(s_f4_f7)
    print("Euclidian division of s_f4_f7 with polynomials of G:")
    print(reduced(s_f4_f7, G, order='lex'))

    print("s_poly of the polynomials f4 f8")
    s_f4_f8 = s_polynomial(f4, f8, 'lex')
    print(s_f4_f8)
    print("Euclidian division of s_f4_f8 with polynomials of G:")
    print(reduced(s_f4_f8, G, order='lex'))

    print("s_poly of the polynomials f4 f9")
    s_f4_f9 = s_polynomial(f4, f9, 'lex')
    print(s_f4_f9)
    print("Euclidian division of s_f4_f9 with polynomials of G:")
    print(reduced(s_f4_f9, G, order='lex'))

    print("s_poly of the polynomials f4 f_1_0")
    s_f4_f_1_0 = s_polynomial(f4, f_1_0, 'lex')
    print(s_f4_f_1_0)
    print("Euclidian division of s_f4_f_1_0 with polynomials of G:")
    print(reduced(s_f4_f_1_0, G, order='lex'))

    print("s_poly of the polynomials f4 f_1_1")
    s_f4_f_1_1 = s_polynomial(f4, f_1_1, 'lex')
    print(s_f4_f_1_1)
    print("Euclidian division of s_f4_f_1_1 with polynomials of G:")
    print(reduced(s_f4_f_1_1, G, order='lex'))

    print("s_poly of the polynomials f4 f_1_2")
    s_f4_f_1_2 = s_polynomial(f4, f_1_2, 'lex')
    print(s_f4_f_1_2)
    print("Euclidian division of s_f4_f_1_2 with polynomials of G:")
    print(reduced(s_f4_f_1_2, G, order='lex'))

    print("s_poly of the polynomials f4 f_1_3")
    s_f4_f_1_3 = s_polynomial(f4, f_1_3, 'lex')
    print(s_f4_f_1_3)
    print("Euclidian division of s_f4_f_1_3 with polynomials of G:")
    print(reduced(s_f4_f_1_3, G, order='lex'))

    print("s_poly of the polynomials f5 f6")
    s_f5_f6 = s_polynomial(f5, f6, 'lex')
    print(s_f5_f6)
    print("Euclidian division of s_f5_f6 with polynomials of G:")
    print(reduced(s_f5_f6, G, order='lex'))

    print("s_poly of the polynomials f5 f7")
    s_f5_f7 = s_polynomial(f5, f7, 'lex')
    print(s_f5_f7)
    print("Euclidian division of s_f5_f7 with polynomials of G:")
    print(reduced(s_f5_f7, G, order='lex'))

    print("s_poly of the polynomials f5 f8")
    s_f5_f8 = s_polynomial(f5, f8, 'lex')
    print(s_f5_f8)
    print("Euclidian division of s_f5_f8 with polynomials of G:")
    print(reduced(s_f5_f8, G, order='lex'))

    print("s_poly of the polynomials f5 f9")
    s_f5_f9 = s_polynomial(f5, f9, 'lex')
    print(s_f5_f9)
    print("Euclidian division of s_f5_f9 with polynomials of G:")
    print(reduced(s_f5_f9, G, order='lex'))

    print("s_poly of the polynomials f5 f_1_0")
    s_f5_f_1_0 = s_polynomial(f5, f_1_0, 'lex')
    print(s_f5_f_1_0)
    print("Euclidian division of s_f5_f_1_0 with polynomials of G:")
    print(reduced(s_f5_f_1_0, G, order='lex'))

    print("s_poly of the polynomials f5 f_1_1")
    s_f5_f_1_1 = s_polynomial(f5, f_1_1, 'lex')
    print(s_f5_f_1_1)
    print("Euclidian division of s_f5_f_1_1 with polynomials of G:")
    print(reduced(s_f5_f_1_1, G, order='lex'))

    print("s_poly of the polynomials f5 f_1_2")
    s_f5_f_1_2 = s_polynomial(f5, f_1_2, 'lex')
    print(s_f5_f_1_2)
    print("number of polynomials of G:")
    print(len(reduced(s_f5_f_1_2, G, order='lex')[0]))
    print("Euclidian remained of the division of s_f5_f_1_2 with polynomials of G:")
    print(reduced(s_f5_f_1_2, G, order='lex')[1])

    print("s_poly of the polynomials f5 f_1_3")
    s_f5_f_1_3 = s_polynomial(f5, f_1_3, 'lex')
    print(s_f5_f_1_3)
    print("number of polynomials of G:")
    print(len(reduced(s_f5_f_1_3, G, order='lex')[0]))
    print("Euclidian remained of the division of s_f5_f_1_3 with polynomials of G:")
    print(reduced(s_f5_f_1_3, G, order='lex')[1])

    print("s_poly of the polynomials f6 f7")
    s_f6_f7 = s_polynomial(f6, f7, 'lex')
    print(s_f6_f7)
    print("number of polynomials of G:")
    print(len(reduced(s_f6_f7, G, order='lex')[0]))
    print("Euclidian remained of the division of s_f6_f7 with polynomials of G:")
    print(reduced(s_f6_f7, G, order='lex')[1])

    print("s_poly of the polynomials f6 f8")
    s_f6_f8 = s_polynomial(f6, f8, 'lex')
    print(s_f6_f8)
    print("number of polynomials of G:")
    print(len(reduced(s_f6_f8, G, order='lex')[0]))
    print("Euclidian remained of the division of s_f6_f8 with polynomials of G:")
    print(reduced(s_f6_f8, G, order='lex')[1])

    print("s_poly of the polynomials f6 f9")
    s_f6_f9 = s_polynomial(f6, f9, 'lex')
    print(s_f6_f9)
    print("number of polynomials of G:")
    print(len(reduced(s_f6_f9, G, order='lex')[0]))
    print("Euclidian remained of the division of s_f6_f9 with polynomials of G:")
    print(reduced(s_f6_f9, G, order='lex')[1])

    print("Euclidian remained of the division of s_f6_f_1_0 with polynomials of G:")
    print(reduced(s_polynomial(f6, f_1_0, 'lex'), G, order='lex')[1])
    print("Euclidian remained of the division of s_f6_f_1_1 with polynomials of G:")
    print(reduced(s_polynomial(f6, f_1_1, 'lex'), G, order='lex')[1])
    print("Euclidian remained of the division of s_f6_f_1_2 with polynomials of G:")
    print(reduced(s_polynomial(f6, f_1_2, 'lex'), G, order='lex')[1])
    print("Euclidian remained of the division of s_f6_f_1_3 with polynomials of G:")
    print(reduced(s_polynomial(f6, f_1_3, 'lex'), G, order='lex')[1])
    print(" ")
    for i, f in enumerate([f8, f9, f_1_0, f_1_1, f_1_2, f_1_3]):
        print("Euclidian remained of the division of s_f7_f"+ str(8+i) + " with polynomials of G:")
        print(reduced(s_polynomial(f7, f, 'lex'), G, order='lex')[1])
    print(" ")
    for i, f in enumerate([f9, f_1_0, f_1_1, f_1_2, f_1_3]):
        print("Euclidian remained of the division of s_f8_f"+ str(9+i) + " with polynomials of G:")
        print(reduced(s_polynomial(f8, f, 'lex'), G, order='lex')[1])
    print(" ")
    for i, f in enumerate([f_1_0, f_1_1, f_1_2, f_1_3]):
        print("Euclidian remained of the division of s_f9_f" + str(10 + i) + " with polynomials of G:")
        print(reduced(s_polynomial(f9, f, 'lex'), G, order='lex')[1])

    print(" ")
    for i, f in enumerate([f_1_1, f_1_2, f_1_3]):
        print("Euclidian remained of the division of s_f_1_0_f" + str(11 + i) + " with polynomials of G:")
        print(reduced(s_polynomial(f_1_0, f, 'lex'), G, order='lex')[1])

    print(" ")
    for i, f in enumerate([f_1_2, f_1_3]):
        print("Euclidian remained of the division of s_f_1_1_f" + str(12 + i) + " with polynomials of G:")
        print(reduced(s_polynomial(f_1_1, f, 'lex'), G, order='lex')[1])

    print(" ")
    for i, f in enumerate([f_1_3]):
        print("Euclidian remained of the division of s_f_1_2_f" + str(13 + i) + " with polynomials of G:")
        print(reduced(s_polynomial(f_1_2, f, 'lex'), G, order='lex')[1])
    print("F:")
    print(F)
    print("G:")
    for g in G:
        print(g)

def ex_4_four_var():
    """
    Fail for the thoery that we only have to use originel polynomials...
    :return:
    """
    F = [x**2 + z + 2*x*t , t**2  + 2*x*y - (t**3) , z + x*y*t - y*z*t]
    f1, f2, f3 = F[0], F[1], F[2]
    print("f1 leading monomial:")
    print(LM(f1, order='lex'))
    print("f2 leading monomial:")
    print(LM(f2, order='lex'))
    print("f3 leading monomial:")
    print(LM(f3, order='lex'))
    G = [ f1, f2, f3]

    for i,f in enumerate(G):
        print("1, " + str(1+ i))
        #print("Euclidian remained of the division of s_f1_f" + str(1 + i) + " with polynomials of G:")
        r = reduced(s_polynomial(f1, f, 'lex'), G, order='lex')[1]
        if r != 0:
            G.append(r)
        if r == 0:
            print("Euclidian remained of the division of s_f1_f" + str(1 + i) + " with polynomials of G = 0 !")

    for i,f in enumerate(G):
        print("2, " + str(1+i))
        #print("Euclidian remained of the division of s_f1_f" + str(2 + i) + " with polynomials of G:")
        r = reduced(s_polynomial(f2, f, 'lex'), G, order='lex')[1]
        if r != 0:
            G.append(r)
        if r == 0:
            print("Euclidian remained of the division of s_f2_f" + str(1 + i) + " with polynomials of G = 0 !")
    for i,f in enumerate(G):
        print("3, " + str(1+i))
        #print("Euclidian remained of the division of s_f1_f" + str(1 + i) + " with polynomials of G:")
        r = reduced(s_polynomial(f3, f, 'lex'), G, order='lex')[1]
        if r != 0:
            G.append(r)
        if r == 0:
            print("Euclidian remained of the division of s_f3_f" + str(1 + i) + " with polynomials of G = 0 !")

    for j in range(3,len(G)):
        for i,f in enumerate(G):
        # print("Euclidian remained of the division of s_f1_f" + str(1 + i) + " with polynomials of G:")
            r = reduced(s_polynomial(G[j], f, 'lex'), G, order='lex')[1]
            if r != 0:
                G.append(r)
                print("polynomial numbers:")
                print(j)
                print(i)
                print("Fail witness :" + str(3 + i))
                print("First polynomial:")
                print(G[j])
                print("Second polynomial:")
                print(f)
                print("remained")
                print(r)
            #if r == 0:
                #print("Euclidian remained of the division of s_f"+str(1+j)+"_f" + str(1 + i) + " with polynomials of G = 0 !")
    print("the end")




if __name__ == '__main__':
    ex_1_two_var()
    #ex_2_three_var()
    #ex_3_three_var()
    #ex_4_four_var()

