FROM ocaml/opam:ubuntu-23.04-ocaml-4.14

RUN sudo apt install libgmp-dev -y
RUN opam install dune zarith menhir

COPY --chown=opam ./ grobner/
WORKDIR /home/opam/grobner
RUN rm -rf _build
RUN opam exec -- dune build test/
RUN 

ENTRYPOINT ["opam", "exec", "--", "dune", "exec", "--"]
